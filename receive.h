//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "utils.h"
#include "version.h"
#include "config.h"

#define set_bitrate(x) _Generic((x), char *: set_bitrate_c, default: set_bitrate_i)(x)

unsigned char debug = 0; //Determines whether the [DEBUG] messages are printed or not
unsigned char autobitrate = 1; //Determines whether the bitrate adjusts automatically
int error_rate = 0; //A variable to determine when the bitrate should change
unsigned char *data; //The data array (bytes)
unsigned char *received_packets; //An array to store info about which packets were received (correctly)
unsigned char confirm[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0}; //First four bytes are the expected packet, the rest is the requested timeout
unsigned char frame_size = 8; //Frame size
unsigned long long dest_file_size; //Size of the destination file (in bytes)
char filename_buffer[1024]; //String for the filename
