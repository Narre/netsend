//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#define SOURCE_PORT 8176
#define PORT 8175

#define DISABLE_AUTOMATIC_BITRATE
