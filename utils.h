//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <time.h>
#include <string.h>
#include<stdio.h>

//Function to check whether the first len characters of two strings are equal, 0 for all characters
unsigned char str_equal(const char *restrict s1, const char *restrict s2, int len);

//Computes the CRC of a packet
unsigned int comp_crc(const unsigned char *buf, size_t len);

//Simple 1-byte hash function
unsigned char comp_hash(const unsigned char *input, unsigned long long length);

//Puts the current thread to sleep for time μs
void delay(int time);

//Stores a 32bit value (no) into an array (ret) of 4 bytes
void int_to_bytes(unsigned int no, unsigned char *ret);

//Returns the value of four consecutive bytes
unsigned int bytes_to_int(unsigned char *ret);
