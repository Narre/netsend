//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "receive.h"

/******FUNCTIONS******/

//Function to replace all slashes in the filename with underscores
static void replace_slashes() {
	for(register unsigned int i = 0; i < strlen(filename_buffer); ++i) if(filename_buffer[i] == '/') filename_buffer[i] = '_';
}

//Checks the error rate and adjusts the bitrate accordingly
static void update_timeout() {
	if(error_rate > 10) error_rate = 10;
	if(error_rate < -10) error_rate = -10;
	if(debug) printf("[DEBUG]<Update_Timeout> Current error rate is %i.\n", error_rate);
	if(error_rate == 10) {
		int_to_bytes((bytes_to_int(&confirm[4]) + 150), &confirm[4]);
		if(debug && bytes_to_int(&confirm[4]) < 1000000) printf("[DEBUG]<Update_Timeout> Requesting lower bitrate...\n");
	} else if(error_rate == -10) {
		int_to_bytes(((unsigned int)(bytes_to_int(&confirm[4]) - 150)), &confirm[4]);
		if(debug && bytes_to_int(&confirm[4]) > 150) printf("[DEBUG]<Update_Timeout> Requesting higher bitrate...\n");
	}
	if(bytes_to_int(&confirm[4]) <= 150) int_to_bytes(150, &confirm[4]);
	if(bytes_to_int(&confirm[4]) >= 1000000) int_to_bytes(1000000, &confirm[4]);
	return;
}

//Sets the bitrate to a fixed value (directly)
static unsigned char set_bitrate_i(int value) {
	unsigned char ret = 1;
	if(!value) {
		printf("[ERROR]<Set_Bitrate> Value \"0\" is too small. Setting to 1.\n");
		value = 1;
		ret = 0;
	} else if(value > 6666) {
		printf("[ERROR]<Set_Bitrate> Value \"%i\" is too large. Setting to 6666.\n", value);
		value = 6666;
		ret = 0;
	}
	int_to_bytes(1000000u / value, &confirm[4]);
	return ret;
}

//Sets the bitrate to a fixed value (reads from "--bitrate=VALUE")
static unsigned char set_bitrate_c(const char *restrict value) {
	if(strlen(value) == 10) { //Error if the string is "--bitrate=" without any value
		printf("[ERROR]<Set_Bitrate> \"\" is not a valid unsigned integer.\n");
		autobitrate = 1;
		return 0;
	}
	unsigned int bitrate = 0;
	for(register int i = strlen(value) - 1, j = 1; i >= 10; --i, j *= 10) { //Calculate the integer value from the given string
		if(value[i] < '0' || value[i] > '9') {
			#ifndef DISABLE_AUTOMATIC_BITRATE
				printf("[ERROR]<Set_Bitrate> \"%s\" is not a valid unsigned integer. Setting to AUTO...\n", &value[10]);
				autobitrate = 1;
			#else
				printf("[ERROR]<Set_Bitrate> \"%s\" is not a valid unsigned integer. Setting to 1024 kB/s...\n", &value[10]);
				set_bitrate(1024);
			#endif
			return 0;
		}
		bitrate += (value[i] - '0') * j;
	}
	return set_bitrate(bitrate);
}

//Save packet *packet no. no of size size into the data array
static void save_packet(unsigned int no, const unsigned char *restrict packet, int size) {
	if(debug) printf("[DEBUG]<Save_Packet> Saving packet %u of size %i.\n", no, size - 16);
	for(register int i = 0; i < size - 16; ++i) data[1000 * no + i] = packet[i + 16];
}

//Print the current progress
static void update_progress(unsigned int packet_no) {
	static unsigned char progress[20] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	static unsigned char i = 0;
	if(i > 20) return;
	for(; i < 20; ++i) if(!progress[i]) break;
	unsigned int status = (unsigned int)((double)packet_no / (double)(dest_file_size / 1000) * 100);
	if(status >= 5 * i) {
		printf("[INFO]<Update_Progress> %u%% completed.\n", 5 * i);
		progress[i] = 1;
		if(i == 20) ++i;
	}
}

//Get the last consecutive saved packet
static int get_last(unsigned int size) {
	static unsigned int i = 0;
	for(; i < size; ++i) if(!received_packets[i]) return i - 1;
	return 0;
}

int main(int argc, char **argv) {

	/******STARTUP******/

	printf("NetSend %s\nCopyleft 🄯 %s Jiří Paleček\n", VERSION, COPYLEFT);
	printf("This program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions. See the LICENCE file for more details.\n\n");

	if(argc > 1 && (str_equal(argv[1], "-h", 0) || str_equal(argv[1], "--help", 0))) { //Print help and exit (on -h or --help)
		printf("[HELP] Usage: %s [ARGUMENTS]\n[HELP]\n", argv[0]);
		printf("[HELP] Possible arguments:\n");
		#ifdef DISABLE_AUTOMATIC_BITRATE
			printf("[HELP] --bitrate=VALUE   Set the bitrate to VALUE kB/s, default is 1024.\n");
		#else
			printf("[HELP] --bitrate=VALUE   Set the bitrate to VALUE kB/s, default is AUTO.\n");
		#endif
		printf("[HELP] --debug           Show all debugging information.\n");
		return 2;
	}

	register unsigned int i;
	register unsigned long long j;

	//Parse arguments
	for(i = 1; i < (unsigned int)argc; ++i) {
		if(str_equal(argv[i], "--debug", 0)) {
			if(debug) {
				printf("[ERROR]<Main> Debug is already set. Ignoring...\n");
				continue;
			} else debug = 1;
		} else if(str_equal(argv[i], "--bitrate=AUTO", 0)) {
			autobitrate = 1;
			printf("[INFO]<Set_Bitrate> Setting bitrate to AUTO\n");
		} else if(str_equal(argv[i], "--bitrate=", 10)) {
			autobitrate = 0;
			if(set_bitrate(argv[1])) printf("[INFO]<Set_Bitrate> Setting bitrate to %s\n", &argv[1][10]);
		} else printf("[ERROR]<Main> \"%s\" is not a valid argument. Ignoring...\n", argv[i]);
	}

	#ifdef DISABLE_AUTOMATIC_BITRATE
		//Evaluate AUTO bitrate as 1024kB/s, may be reverted in the future if automatic bitrate is fixed and usable
		if(autobitrate) {
			autobitrate = 0;
			set_bitrate(1024);
		}
	#endif

	printf("[INFO]<Main> Bitrate is set to ");
	if(autobitrate) printf("AUTO.\n");
	else printf("%u kB/s (%u μs).\n", (unsigned int)(1000000 / bytes_to_int(&confirm[4])), bytes_to_int(&confirm[4]));

	/******COMMUNICATION-SETUP******/

	int sockfd;
	unsigned char buffer[1024]; //Buffer for storing the data from each received packet
	unsigned char hash;
	struct sockaddr_in servaddr, cliaddr;

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("[ERROR]<Main> Socket creation failed.");
		exit(EXIT_FAILURE);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&cliaddr, 0, sizeof(cliaddr));

	//IP information
	servaddr.sin_family = AF_INET; //IPv4
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	//Bind the socket with the server address
	if(bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
		perror("[ERROR]<Main> Bind failed");
		exit(EXIT_FAILURE);
	}

	/******LOCAL-SETUP******/

	unsigned int len = sizeof(servaddr), n;
	unsigned long long bytes_received = 0; //The total number of bytes in the received file
	unsigned int exp_crc;
	unsigned int calc_crc;
	unsigned int last = -1u;

	/******COMMUNICATION-START******/

	while(1) {
		n = recvfrom(sockfd, filename_buffer, 1024, MSG_WAITALL, (struct sockaddr *)&cliaddr, &len); //Get the filename
		if(comp_hash((unsigned char *)filename_buffer, n - 1) == (unsigned char)filename_buffer[n - 1]) break;
	}
	if(n > 1) filename_buffer[n - 1] = '\0'; //Make the filename char array a string
	else {filename_buffer[0] = ' '; filename_buffer[1] = '\0';} //Or, if it's empty, make it a space. (and a string)
	replace_slashes(); //Make sure there are no slashes in the filename
	printf("[INFO]<Main> Establishing connection...\n");
	int_to_bytes(0, &confirm[0]);
	if(autobitrate)int_to_bytes(1500, &confirm[4]);
	confirm[8] = comp_hash(confirm, 8);
	for(i = 0; i < 20; ++i) //Send request to confirm receiving the filename and to set the bitrate/timeout
		sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));

	/******MAIN-COMMUNICATION******/

	for(unsigned char start = 0;;) {
		n = recvfrom(sockfd, (char *)buffer, 1024, MSG_WAITALL, (struct sockaddr *)&cliaddr, &len); //Get a packet
		if(!start) {
			printf("[INFO]<Main> Connection established.\n");
			++start;
		}
		if(n == 1) break;
		if(n < 17 || bytes_to_int(&buffer[1]) > last + frame_size) {
			printf("[ERROR]<Main> Received an invalid packet, ignoring...\n");
			if(autobitrate) {
				error_rate += 2;
				update_timeout();
			}
			confirm[8] = comp_hash(confirm, 8);
			sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
			continue;
		}
		if(debug) printf("[DEBUG]<Main> Received packet %i.\n", bytes_to_int(&buffer[1]));
		if(last != -1u && received_packets[bytes_to_int(&buffer[1])]) { //Check if the packet is not already saved
			if(debug) printf("[DEBUG]<Main> Received duplicite packet, re-sending request, expecting packet %u.\n", bytes_to_int(confirm));
			if(autobitrate) {
				error_rate += 2;
				update_timeout();
			}
			confirm[8] = comp_hash(confirm, 8);
			sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
			continue;
		} else if(bytes_to_int(&buffer[1]) > last + 1) { //Check if some packet were skipped
			if(debug) printf("[DEBUG]<Main> Some packets were skipped. Requesting packet %u.\n", bytes_to_int(&confirm[0]));
			confirm[8] = comp_hash(confirm, 8);
			sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
		} else if(autobitrate) { //If the last packet was received perfectly, adjust the error rate accordingly
			--error_rate;
			update_timeout();
		}
		exp_crc = bytes_to_int(&buffer[6]); //Get the expected CRC
		for(i = 6; i < 10; ++i) buffer[i] = 0; //Clear the CRC bytes in the array
		calc_crc = comp_crc(buffer, n); //Calculate the CRC

		if(calc_crc == exp_crc) { //CRC match
			if(start == 1) { //Set the hash and the filesize on receiving the first valid packet
				last = 0; //Set the last consecutive sent packet to 0
				frame_size = buffer[0]; //Get the frame size
				hash = buffer[5]; //Set the hash
				dest_file_size = 1000 * bytes_to_int(&buffer[10]);
				buffer[12] = 0; buffer[13] = 0;
				dest_file_size += bytes_to_int(&buffer[12]); //Set the filesize
				data = (unsigned char *)malloc(dest_file_size * sizeof(unsigned char));
				received_packets = (unsigned char *)malloc(dest_file_size * sizeof(unsigned char));
				for(i = 1; i < dest_file_size; ++i) received_packets[i] = 0;
				++start;
			}
			update_progress(bytes_to_int(&buffer[1])); //Update progress (percentage)
			received_packets[bytes_to_int(&buffer[1])] = 1; //Mark the packet as saved
			last = get_last(dest_file_size); //Update which packet is the last consecutive saved one
			int_to_bytes(last + 1, &confirm[0]);
			confirm[8] = comp_hash(confirm, 8);
			sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
		} else { //CRC mismatch
			if(debug) printf("[DEBUG]<Main> Packet no. %u: Expected CRC is %X, calculated CRC is %X\n", bytes_to_int(&buffer[1]), exp_crc, calc_crc);
			if(debug) printf("[DEBUG]<Main> Requesting resend...\n");
			if(autobitrate) {
				error_rate += 2;
				update_timeout();
			}
			confirm[8] = comp_hash(confirm, 8);
			sendto(sockfd, (const unsigned char *)confirm, 9, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
			continue;
		}
		save_packet(bytes_to_int(&buffer[1]), buffer, n); //Save the packet into the data array
		bytes_received += n - 16llu; //Keep track of received bytes
	}

	/******DATA-CHECK******/

	printf("[INFO]<Main> Received %llu bytes.\n", bytes_received);
	unsigned char fin_hash = comp_hash(data, bytes_received);
	if(debug) printf("[DEBUG]<Main> Expected hash is %i, calculated hash is %i.\n", (int)hash, (int)fin_hash);
	if(hash != fin_hash) printf("[ERROR]<Main> An error seems to have occured. If the file is corrupted, try again.\n[NOTE] Make sure you are using compatible verions of NetSend on the sending and receiving sides.\n");

	/******END******/

	FILE* dest_file = fopen((char *)filename_buffer, "wb"); //Create the file to write the received data into
	printf("[INFO]<Main> Saving as \"%s\".\n", filename_buffer);
	for(j = 0; j < bytes_received; ++j) fputc_unlocked((int)(data[j]), dest_file); //Write individual bytes into the file
	fclose(dest_file);
	printf("[INFO]<Main> Terminating...\n");
	free(data);
	free(received_packets);
	close(sockfd);
	return 0;
}
