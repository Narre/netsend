NetSend is an application for sending files over the network. It is free software distributed under the GNU General Public Licence v.3 (see LICENCE for more details).

**Compilation:**\
-NetSend comes with a Makefile. Compile the program using GNU Make `make`. You can use `make clean` to remove the compiled and linked files and `make install` to install the program to /usr/bin/\
-If you want to compile the source code manually, be aware that you must use a compiler that supports the `gnu11` or `gnu17` standards. If you want to compile the code using the `c11` or `c17` standard, you will have to make slight modifications to some used functions (namely `putc_unlocked()` -> `putc()` and `getc_unlocked()` -> `getc()`). To compile using the `c99` or `gnu99` standard, you will have to make further modifications to the code (get rid of `_Generic`). To compile `send`, you will also have to link POSIX threads (`-lpthread`). Replacing `pthread.h` with the C11 threads `threads.h` is planned for future versions.

**Usage:**\
-To send files, run `receive` on the receiving side and `send FILE IP_ADDRESS DEST_FILENAME` on the sending side. `FILE` is the name of the file you wish to send, `IP_ADDRESS` is the IPv4 address of the receiving device and `DEST_FILENAME` is the name you want to send the file as.\
-Possible arguments for `receive` are `--debug` to show all debugging info, and `--bitrate=VALUE` to set the transfer speed.\
-Possible arguments for `send` are `--debug` to show all debugging info, and `--frame=VALUE` to set the number of packets sent before checking for response.\
-You can use `--help` or `-h` with either to print the usage information of NetSend.

**Versioning**\
-NetSend started at version 1.0.0 (that was not a great choice since it didn't even do much - like send files over 999 bytes, but since retroactively renumbering older versions is a monumentally stupid idea, it will stay that way).\
-Until version 1.3.6, the number always increased by one with each release (and often with every commit), ignoring the *points*. So version 1.0.9, for example, went straight to 1.1.0, regardless of the significance (or rather insignificance, since the functionality was identical) of the changes. That's not a great system, so...\
-From version 1.3.6, NetSend switched to the standard GNU versioning (*major.minor.revision*). Revisions are versions where the code is optimised or cleaned up, but the core functionality is unchanged and the versions are therefore compatible with each other. Minor versions are versions where some functionality changes make the new version incompatible with the older ones (new features, packet format changes etc.). Major versions (there are currently no specific plans for a version 2.0.0) would be versions with changes so significant that they deserve a major version number increase. The first increase of the major version is not currently planned specifically, but it could well be the first versions that incorporates sliding windows, splitting windows, selective repeat and a more complex file hash function.

**Compatibility:**\
-Prior to version 1.3.6, the individual versions are not compatible with each other. Since version 1.3.6, versions of the same *minor* version number (major.minor.revision) should always be compatible. Since the file hash function changed in 1.3.9, other 1.3.x versions will detect a hash mismatch if paired with 1.3.9. This, however, will not break compatibility and/or result in corrupted files, only print a warning, but since there is no way of knowing whether the hash matches, it is not recommended to couple 1.3.9 with other 1.3.x versions on sketchy connections. With that being said, since each packet uses a much more complex CRC function, no actual errors should go through either way.

**Platform support:**\
-NetSend was made with Linux in mind and it was only tested on Linux. It might be possible to compile the program on other platforms as well, but some libraries would probably have to be replaced and the code modified.

**Contact:**\
-If you want to report a bug, make a suggestion or directly contribute to the project, you can find it on GitLab: `https://gitlab.com/Narre/netsend`\
-If you wish to contact the author directly, you can write an e-mail to `narre@protonmail.com`. However, unless you have a really, really good reason to do that, please use issues on GitLab.
