#  NetSend
#  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public Licence as published by
#  the Free Software Foundation, either version 3 of the Licence, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU General Public Licence for more details.
#
#  You should have received a copy of the GNU General Public Licence
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

CC=gcc
CFLAGS=-Wall -Wextra -Werror -std=gnu17

all: send receive

send.o: send.c send.h utils.h version.h config.h
	$(CC) $(CFLAGS) send.c -c -o send.o

receive.o: receive.c receive.h utils.h version.h config.h
	$(CC) $(CFLAGS) receive.c -c -o receive.o

utils.o: utils.c utils.h
	$(CC) $(CFLAGS) utils.c -c -o utils.o

send: send.o utils.o
	$(CC) $(CFLAGS) send.o utils.o -lpthread -o send

receive: receive.o utils.o
	$(CC) $(CFLAGS) receive.o utils.o -o receive 

clean:
	rm -f send receive send.o receive.o utils.o

install: send receive
	cp receive /usr/bin/
	cp send /usr/bin/
