NetSend is an application for sending files over the network. It is free software distributed under the Gnu General Public Licence v.3 (see LICENCE for more details).

**Author:**
- Jiří 'Narre' Paleček

**Contribution to testing:**
- Oleksandra Kocyba

**Acknowledgement:**
- Lukáš Krob

\
**How to contribute yourself**\
If you wish to contribute to the NetSend project, you can do so on the NetSend GitLab page: `https://gitlab.com/Narre/netsend/`

If you wish to report a bug or submit an idea, use Issues at `https://gitlab.com/Narre/netsend/-/issues/`\
If you wish to contribute your own code, feel free to make a fork of the project, create a branch, inplement or fix something, and make a merge request.

**Filing an issue**\
If you decide to file an issue, please follow these guidelines, otherwise your issue will likely be rejected and marked as invalid.
- If possible, use the provided template to keep track of important factors (you can remove individual entries, for example, if you want to submit an idea, you don't have to include information about your system, but make sure you don't remove any information that may be important to the issue).
- Write your issues in English. If you use another language, chances are nobody is going to understand you (especially an online translator). If we can't understand your issues, they will not be considered.

**Contributing code**\
If you decide to contribute your code, please follow these guidelines, otherwise you will either be told to fix the code, or your merge request will be rejected.
- Please follow the coding style of NetSend. We understand that each programmer has their own style of coding - indentation, placement of brackets and newlines etc. But we don't want to have our program be an exhibition of all kinds of exotic coding styles.
- Make sure that you choose descriptive but not too long names for new variables, functions and modules.
- *Indentation* - always use tabs, not spaces. One tab for one indentation level. Tabs are *really* underrated in coding. Since every programmer can set the tab width to their liking in their preferred editor without changing how the code looks for others, it is (in our opinion) the superior way to indent any code.
- *Brackets* - Please make sure all `{`'s are at the end of the first line, not on a new line, and that all `}`'s are on a new line after the end of the block of code. Make sure the blocks using `(`, `)`, `[` and `]` characters are all on the same line and not split between multiple lines unless it **greatly** improves the readability of the particular block. There is no specific reason for this other than that's the style we went for and we want to keep it consistent.
- *Line splitting* - Please do not split a line of code *too* much. We understand this is not a common view, but since most people today have more horizontal space on their screens than vertical space, not splitting lines makes it possible for the programmers to see more of the code without having to scroll through it. That doesn't mean that you should make lines that are tens of thousands of characters long, but don't split lines just because they reach an arbitrary, usually unnecessarily small, number of characters.
- *On the note of vertical compactness* - please always keep the data type, the name, the parameters and the opening `{` of functions on the same line. This does not apply to structs and enums, where we would prefer the individual entries on separate lines for better readability.
- *Switches* - If you make a switch, please keep each case on a single line if its code is short, even if it consists of two or three commands. That is not great practice outside of switches, but since switches are notoriously demanding for vertical real estate, this can dramatically improve the overall readability of the entire part of the code.
- *Naming variables, constants, data types and functions* - For preprocessor constants (`#define`s), please use `UPPER_CASE` and underscores to separate words. For everything else, use `lower_case` and underscores (that includes `const`s). Do not use different standards (camel case, upper camel case, etc.). Please make the names sufficiently descriptive but not too long to make them a pain to type in a plain text editor. For example, `sgewarlt` is not descriptive, others won't know what the variable is for, `a_small_grey_elephant_with_a_really_long_trunk` is too long and a pain to type. Use something like `small_elephant` instead. The code does not (and should not) have to be recreatable just from the names of variables and functions.
- *Primitive data types* - Please always type data types in their entirety, eg. `unsigned int`, instead of just `unsigned` or `uint16` or whatnot, but don't write int alongside shorts and longs, that might be confusing, so use `long long`, not `long long int`. Please use `long long`s, not `long`s to improve compatibility. As of lately, gcc apparently likes to make `long`s only 4 bytes. As much as we'd prefer using just `long`s and have them be 8 bytes long, using `long long`s ensures this. Since this program is Linux-only, you can assume (for the most part) that the sizes are 1 byte for char, 2 bytes for short, 4 bytes for int, 8 bytes for long long, 4 bytes for float and 8 bytes for double.
- *Pointers* - When declaring a pointer, please put the `*` after the space, and don't put a space between the `*` and the variable name, for example, use `int *my_pointer`, not `int*my_pointer`, `int* my_pointer` or `int * my_pointer`. Similarly, if you use unique pointers, use `int *restrict my_pointer`, not `int * restrict my_pointer` etc.
- *Used data types* - Please always use the smallest data type that you are sure will suffice. Using longs for a loop which only counts to 200 is extremely inefficient - you would use an `unsigned char` in that case. As for signedness, always use `unsigned` unless the variable can go into the negatives. It will make it easier for others to determine what the variable is for if they immediately know whether it can or can't go below zero.
- *Bools* - Do not declare bools anywhere, or import bool-defining libraries. It's unnecessary since bools take a whole byte anyway. Use `unsigned char` instead. It also makes it easier to extend the variable to three or more states in the future if need be. Similarly, use `0` and `1`, NOT `false` and `true`.
- *Comparison* - Please use `!` when comparing a whole data type to zero, not `== 0`
- *Increments/decrements* - Please always use `++`/`--` for increments and decrements. Also, always use pre-increments / pre-decrements unless a post-increment / post-decrement is required.
- *On a similar note* - always use `x += y`, `x <<= y` etc. instead of `x = x + y`, `x = x << y` etc. This is C, not [insert a weird exotic language here]
- *Keywords* - Always use keywords such as `noreturn` wherever applicable (Actually, you probably won't use that one `:)`). It slightly improves the readability of the code. Also try to use `static` functions as much as possible. Such functions can't have their prototypes in the headers, but if said function isn't meant to be accessed from other modules, this will improve the readability of the header and prevent others from mistaking your should-be-static function for a utility function.
- *Optimisation* - Try to write the code as optimally as possible, but not at the cost of readability. For example, use `register` variables for loops, and declare index variables for loops at the beginning of a block to avoid having to create a new variable every time a loop is entered. Similarly, try to limit the number of declarations inside loops, unless doing it would cause excessive memory usage. But do not overdo it. If your code is a soup of inline assembly and weird hard to read constructs, nobody will bother with it and it certainly won't be merged into the project.
- *Literals* - Please make the data type obvious when using literals, e.g. using `0.f` for floating point literals, `0l` for long literals and `0u` for unsigned literals, if their data type is important (usually always for floats/doubles, and not so much for the rest).
- *Spaces* - Please use spaces consistently with the rest of the code. Describing it here would be too exhausting - just looking at the code will only take you a few minutes. Thank you.
