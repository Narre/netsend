//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "send.h"

/******FUNCTIONS******/

//Get the highest consecutive sent packet
static unsigned int get_last_sent() {
	static unsigned int i = 0;
	for(;; ++i) if(!sent_packets[i]) return i - 1;
}

//Print the current progress
static void update_progress(unsigned int packet_no) {
	static unsigned char progress[20] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	static unsigned char i = 0;
	if(i > 20) return;
	for(; i < 20; ++i) if(!progress[i]) break;
	unsigned int status = (unsigned int)((double)packet_no / (double)(source_file_size / 1000) * 100);
	if(status >= 5 * i) {
		printf("[INFO]<Update_Progress> %u%% completed.\n", 5 * i);
		progress[i] = 1;
		if(i == 20) ++i;
	}
}

void *listen_thread(void *vargp) {
	(void)(vargp);
	unsigned char buffer[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	while(1) {
		int n = recvfrom(sockfd, buffer, 9, MSG_WAITALL, (struct sockaddr *)&servaddr, &len); //Get a response
		if(n) {
			if(n == 1) break; //End
			if(comp_hash(buffer, 8) != buffer[8]) {
				if(debug && !mute_listener) printf("[DEBUG]<Listener> Received an invalid acknowledgement packet. Ignoring...\n");
				if(requested_packet == -1u) requested_packet = 0;
				continue;
			}
			if(bytes_to_int(&buffer[0]) > get_last_sent() + 1) {
				if(debug && !mute_listener) printf("[DEBUG]<Listener> Received request for packet %u but packet %u was not sent yet. %u will be sent.\n", bytes_to_int(&buffer[0]), get_last_sent() + 1, get_last_sent() + 1);
				requested_packet = get_last_sent() + 1; //Sets the requested packet to the first packet that has not been sent yet if the acknowledgement packet is invalid
			} else requested_packet = bytes_to_int(&buffer[0]); //Sets the expected packet to whatever the receiving side is expecting next
			if(debug && !mute_listener)printf("[DEBUG]<Listener> Requested packet %u.\n", requested_packet);
			if(timeout != bytes_to_int(&buffer[4])) { //If current timeout is different from requested timeout, it is updated
				if(bytes_to_int(&buffer[4]) < 150 && !mute_listener)printf("[ERROR]<Listener> The requested bitrate (%ukB/s) is too high. Ignoring...\n", (unsigned int)(1000000 / bytes_to_int(&buffer[4])));
				else if(bytes_to_int(&buffer[4]) > 1000000 && !mute_listener) printf("[ERROR]<Listener> The requested bitrate (%ukB/s) is too low. Ignoring...\n", (unsigned int)(1000000 / bytes_to_int(&buffer[4])));
				else {
					timeout = bytes_to_int(&buffer[4]); //Update the timeout
					if(debug && !mute_listener)printf("[DEBUG]<Listener> Adjusting timeout to %uμs.\n", timeout);
				}
			}
		}
	}
	delay(1000);
	if(debug) printf("[DEBUG]<Listener> Terminating...\n");
	return NULL;
}

//Function to send packet no. no with hash hash from the data array
static int send_packet(int no, unsigned char hash) { //Function to send packet no. no with hash hash from the data array
	sent_packets[no] = 1; //Mark the packet as sent
	update_progress(no); //Update progress (percentage)
	unsigned char current_packet[1016]; //The packet to be sent
	unsigned int no_of_bytes = 1000; //The number of bytes in the current packet
	register unsigned int i;
	if(data_size - 1000ull * no < 1000) no_of_bytes = (unsigned int)(data_size - 1000ull * no);
	if(1000ull * no > data_size) { //If there is no data left to be sent, send END 20 times
		for(i = 0; i < 20; ++i) sendto(sockfd, (const unsigned char *)end, 1, MSG_CONFIRM, (const struct sockaddr *)&servaddr, sizeof(servaddr));
		return 1; //Return value to indicate the last packet being sent
	}
	for(i = 0; i < 16; ++i) current_packet[i] = 0u; //Fill the first 16 bytes with zeroes
	current_packet[0] = (unsigned char)frame_size;
	for(i = 16; i < no_of_bytes + 16; ++i) current_packet[i] = data[1000 * no + i - 16]; //Fill the remaining 1000 bytes with the data
	int_to_bytes(no, &current_packet[1]); //Compute the packet number
	current_packet[5] = hash; //Store the hash in byte 5
	int_to_bytes((unsigned int)(source_file_size % 1000), &current_packet[12]);
	int_to_bytes((unsigned int)(source_file_size / 1000), &current_packet[10]); //Write the filesize into the packet (positions 10-15)
	int_to_bytes(comp_crc(current_packet, no_of_bytes + 16), &current_packet[6]); //Calculate the CRC and write it into the packet (positions 6-9)
	sendto(sockfd, (const unsigned char *)current_packet, no_of_bytes + 16, MSG_CONFIRM, (const struct sockaddr *)&servaddr, sizeof(servaddr)); //Send the packet
	return 0; //Return value to indicate there are more packets to be sent
}

//Set frame_size to VALUE from string "--frame=VALUE"
static void set_frame_size(const char *restrict value) {
	static unsigned char run = 0;
	if(run) printf("[ERROR]<Set_Frame_Size> Frame size is already set. Overwriting...\n");
	unsigned int ret = 0;
	if(strlen(value) == 8) {
		printf("[ERROR]<Set_Frame_Size> \"\" is not a valid unsigned integer. Ignoring...\n");
		return;
	}
	for(register unsigned int i = strlen(value) - 1, j = 1; i >= 8; --i, j *= 10) {
		if(value[i] < '0' || value[i] > '9') {
			printf("[ERROR]<Set_Frame_Size> \"%s\" is not a valid unsigned integer. Ignoring...\n", &value[8]);
			return;
		}
		ret += (value[i] - '0') * j;
	}
	if(!ret) {
		printf("[ERROR]<Set_Frame_Size> Value \"0\" is too small. Setting to 1.\n");
		ret = 1;
	} else if(ret > 50) {
		printf("[ERROR]<Set_Frame_Size> Value \"%s\" is too large. Setting to 50.\n", &value[8]);
		ret = 50;
	}
	frame_size = ret;
	run = 1;
}

int main(int argc, char **argv) {

	/******STARTUP******/

	printf("NetSend %s\nCopyleft 🄯 %s Jiří Paleček\n", VERSION, COPYLEFT);
	printf("This program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it\nunder certain conditions. See the LICENCE file for more details.\n\n");

	if(argc < 4) { //Print help and exit
		printf("[HELP] Usage: %s FILE DEST_IP DEST_FILENAME [ARGUMENTS]\n[HELP]\n", argv[0]);
		printf("[HELP] Possible arguments:\n");
		printf("[HELP] --frame=VALUE   Set the frame size to VALUE ∈ <1, 50>.\n");
		printf("[HELP] --debug         Show all debugging information.\n");
		return 2;
	}

	register unsigned int i;

	//Parse arguments
	for(i = 4; i < (unsigned int)argc; ++i) {
		if(str_equal(argv[i], "--debug", 0)) {
			if(debug) {
				printf("[ERROR]<Main> Debug is already set. Ignoring...\n");
				continue;
			}
			debug = 1;
		} else if(str_equal(argv[i], "--frame=", 8)) set_frame_size(argv[i]);
		else printf("[ERROR]<Main> \"%s\" is not a valid argument. Ignoring...\n", argv[i]);
	}

	/******LOCAL_SETUP******/

	FILE* source_file = fopen(argv[1], "rb"); //Open the desired file
	printf("[INFO]<Main> Loading the file...\n");
	if(source_file == NULL) {
		printf("[ERROR]<Main> \"%s\" could not be found. Terminating.\n", argv[1]);
		return 1;
	}
	fseek(source_file, 0, SEEK_END);
	source_file_size = ftell(source_file) + 1; //Get the file size
	fseek(source_file, 0, SEEK_SET);
	if(source_file_size > 4294967295999) {
		printf("[ERROR]<Main> The file is too large, maximum size is 4.29TB (3.9 TiB).\n[INFO]<Main> Terminating...\n");
		fclose(source_file);
		return 3;
	}
	if(source_file_size < 15000) {
		printf("[INFO]<Main> The file is small. Setting frame size to 1.\n");
		frame_size = 1;
	} else if(frame_size > ((int)(source_file_size / 1000)) - 10) {
		frame_size = ((int)(source_file_size / 1000)) - 10;
		printf("[ERROR]<Main> The frame size is too large. Setting to %i.\n", frame_size);
	}
	printf("[INFO]<Main> Frame size is set to %i.\n", frame_size);
	pthread_t listener;
	data = (unsigned char *)malloc(source_file_size * sizeof(unsigned char)); //Allocate the data array to load the source file into
	sent_packets = (unsigned char *)malloc(source_file_size * sizeof(unsigned char)); //Keep track of the packets that were sent
	unsigned char hash;
	for(i = 0; i < source_file_size; ++i) sent_packets[i] = 0; //Fill sent_packets with zeroes
	printf("[INFO]<Main> Reading the file...\n");
	for(i = 0;; ++i) {
		data[i] = (unsigned char)getc_unlocked(source_file); //Read a byte from the source file and write it into the data array
		if(feof(source_file)) {
			data_size = (unsigned long long)i;
			break;
		}
	}
	hash = comp_hash(data, data_size); //Get the hash of packet <data> of size <data_size>
	if(debug) printf("[DEBUG]<Main> The hash is %i.\n", (int)hash);

	/******COMMUNICATION_SETUP******/

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("[ERROR]<Main> Socket creation failed.");
		exit(EXIT_FAILURE);
	}

	//destination IP
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT);
	servaddr.sin_addr.s_addr = inet_addr(argv[2]);

	//source IP
	memset(&cliaddr, 0, sizeof(struct sockaddr_in));
	cliaddr.sin_family = AF_INET;
	cliaddr.sin_port = htons(SOURCE_PORT);
	if (bind(sockfd, (const struct sockaddr *)&cliaddr, sizeof(cliaddr)) < 0) { //Source port bind
		perror("[ERROR]<Main> Bind failed");
		exit(EXIT_FAILURE);
	}

	len = sizeof(servaddr); //Define len to avoid memory errors

	/******COMMUNICATION_START******/

	printf("[INFO]<Main> Establishing connection...\n");
	pthread_create(&listener, NULL, listen_thread, NULL); //Response listener (separate thread)
	if(strlen(argv[3]) > 100) argv[3][100] = '\0'; //Make sure the filename is no longer than 100 characters
	int filename_length = strlen(argv[3]);
	argv[3][strlen(argv[3])] = (char)comp_hash((unsigned char *)argv[3], strlen(argv[3]));
	char try_again = '\0';
	unsigned char tried_again = 0;
	for(i = 0;; ++i) { //Keep sending the filename until confirmed
		sendto(sockfd, (const unsigned char *)argv[3], filename_length + 1, MSG_CONFIRM, (const struct sockaddr *)&servaddr, sizeof(servaddr)); //Send the filename
		delay(10000);
		if(requested_packet != (unsigned int)-1) {
			printf("[INFO]<Main> Connection established.\n");
			break;
		}
		if(i < 15)printf("[ERROR]<Main> Connection error. The communication may fail. Retrying...\n");
		else { //If the connection cannot be established (the IP is wrong, the receiver is irresponsive, etc), terminate
			if(!tried_again) {
				mute_listener = 1;
				printf("[ERROR]<Main> The connection seems not to be working. Try again? (y/n)\n[INPUT]<User> ");
				fscanf(stdin, "%c", &try_again);
				mute_listener = 0;
				if(try_again == 'Y' || try_again == 'y') {
					i = 0;
					++tried_again;
					continue;
				} else if(try_again == '\0') printf("\n");
			}
			printf("[ERROR]<Main> Connection error. Terminating...\n");
			sendto(sockfd, (const unsigned char *)end, 1, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
			pthread_join(listener, NULL);
			free(data);
			free(sent_packets);
			close(sockfd);
			fclose(source_file);
			return 1;
		}
	}
	delay(20000); //Wait so the receiver has enough time to start listening
	printf("[INFO]<Main> Sending %llu bytes to \"%s\"...\n", data_size, argv[2]);

	/******MAIN_COMMUNICATION******/

	i = 0;
	for(register unsigned char timeout_check;;) {
		timeout_check = 0; //Make sure the timeout is not ignored
		if(debug) printf("[DEBUG]<Main> Requested packet %u, sending packet.\n", requested_packet);
		if(send_packet(i, hash)) break; //send_packet returns 1 if the last packet was sent
		if(frame_size > 1 && get_last_sent() < (unsigned int)(source_file_size / 1000) - frame_size - 2) { //Send frame_size packets without checking until frame_size - 2 packets from the end
			for(register unsigned int j = get_last_sent() + 1, k = j; k < j + frame_size; ++k) { //Send a frame
				if(!sent_packets[k]) { //Only send the packet if it is not marked as sent already
					if(debug) printf("[DEBUG]<Main> Sending packet %u.\n", k);
					send_packet(k, hash);
					delay(timeout);
				}
			}
			i = requested_packet; //Update the requested packet
			if(debug && i > get_last_sent()) printf("[DEBUG]<Main> No packets were lost in this frame.\n");
			else if(debug) printf("[DEBUG]<Main> Some packets were lost in this frame. Resending...\n");
			while(i != get_last_sent() + 1) { //Resend all re-requested packets from the frame
				if(debug) printf("[DEBUG]<Main> Re-sending packet %u.\n", i);
				send_packet(i, hash);
				delay(timeout);
				i = requested_packet; //Update the requested packet
			}
			timeout_check = 1; //Make sure the timeout is not double the length after every frame
		}
		if(!timeout_check)delay(timeout); //Wait for timeout μs determined by the bitrate set by the receiving side
		i = requested_packet; //Update the requested packet
	}

	/******END******/

	if(debug) printf("[DEBUG]<Main> Waiting for Listener to terminate...\n");
	sendto(sockfd, (const unsigned char *)end, 1, MSG_CONFIRM, (const struct sockaddr *)&cliaddr, sizeof(cliaddr));
	pthread_join(listener, NULL);
	printf("[INFO]<Main> Terminating...\n");
	free(data);
	free(sent_packets);
	close(sockfd);
	fclose(source_file);
	return 0;
}
