//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "utils.h"
#include "version.h"
#include "config.h"

unsigned int requested_packet = (unsigned int)-1; //A variable to store confirmation / error packets from the receiver (MAX before the connection is established)
int sockfd;
struct sockaddr_in servaddr, cliaddr;
unsigned int len;
unsigned char* data; //The data array
unsigned long long data_size = 0; //The number of unprocessed bytes remaining in the file
unsigned char debug = 0; //Determines whether the [DEBUG] messages are printed or not
const unsigned char end[1] = {255}; //The END packet
unsigned int timeout = 666; //Set the default timeout to 1500μs
unsigned long long source_file_size; //In bytes
unsigned char *sent_packets; //Keep track of sent packets
int frame_size = 8; //Packets per frame
unsigned char mute_listener = 0;

//Listen function for receiving response packets; runs in a separate thread
void *listen_thread(void *vargp);
