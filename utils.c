//  NetSend
//  Copyleft 🄯 2019-2023 Jiří Paleček <narre@protonmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public Licence as published by
//  the Free Software Foundation, either version 3 of the Licence, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public Licence for more details.
//
//  You should have received a copy of the GNU General Public Licence
//  along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "utils.h"

unsigned char str_equal(const char *restrict s1, const char *restrict s2, int len) {
	if(!len) {
		if(strlen(s1) != strlen(s2)) return 0;
		len = strlen(s1) > strlen(s2) ? strlen(s1) : strlen(s2);
	}
	if(strlen(s1) >= (unsigned long long)len && strlen(s2) >= (unsigned long long)len) {
		for(register int i = 0; i < len; ++i) if(s1[i] != s2[i]) return 0;
	} else return 0;
	return 1;
}

static unsigned int *get_table() {
	static unsigned int table[256];
	static unsigned char done = 0;
	if(done) return table;
	register unsigned int i, j;
	unsigned int remainder;
	for(i = 0; i < 256; ++i) {
		remainder = i;
		for(j = 0; j < 8; ++j) {
			if(remainder & 1) {
				remainder >>= 1;
				remainder ^= 0xedb88320;
			} else remainder >>= 1;
		}
		table[i] = remainder;
	}
	done = 1;
	return table;
}

unsigned int comp_crc(const unsigned char *restrict buf, size_t len) {
	unsigned int crc = ~0x0;
	unsigned int *table = get_table();
	unsigned char octet;
	const unsigned char *p, *q;
	q = buf + len;
	for(p = buf; p < q; ++p) {
		octet = *p;
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}
	return ~crc;
}

unsigned char comp_hash(const unsigned char *restrict input, unsigned long long length) {
	unsigned char ret = 0;
	for(register unsigned long long i = 0; i < length; ++i) ret += (unsigned char)input[i];
	return ret;
}

void delay(int time) {
	clock_t start_time = clock();
	while(clock() < start_time + time);
}

void int_to_bytes(unsigned int no, unsigned char *ret) {
	ret[0] = no >> 24;
	ret[1] = (no & 0xff0000) >> 16;
	ret[2] = (no & 0xff00) >> 8;
	ret[3] = no & 0xff;
}

unsigned int bytes_to_int(unsigned char *ret) {
	return (ret[0] << 24) | (ret[1] << 16) | (ret[2] << 8) | ret[3];
}
